package org.communiquons.testbbcodeparser;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import org.communiquons.bbcodeparser.BBCodeParser;
import org.communiquons.bbcodeparser.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        final TextView textView = findViewById(R.id.text);
        render(textView);
        findViewById(R.id.render_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                render(textView);
            }
        });
    }

    private void render(TextView v){
        v.setText(
                new BBCodeParser().parse(
                        "Test: [b]b[u]o[/u]ld[/b] [color=#FF4136]x[sup]2[/sup][/color] nothing "
                                + System.getProperty("line.separator")
                                + "t[sub]i[/sub] t[sub]0[/sub]\n"
                                + "[left]Left text[/left]\n"
                                + "[center]Center text[/center]\n"
                                + "[right]Right text[/right]\n"
                                + "[s]Strikethrough[/s]"
                                + "[ul][li]Option1[/li][li]Option2[/li][/ul]"
                                + "[ol][li]Ordered list 1[/li][li]Ordered list 2[/li][/ol]"
                                + "[quote]If Microsoft ever does applications for Linux it means I've won.[/quote]"
                                + "[code]console.log('Hello world!');[/code]"
                                + " [i]Italic and [u]underline[/u][/i]   [d"));
    }
}
